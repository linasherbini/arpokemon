![alt text](ARPokemon/Assets.xcassets/AppIcon.appiconset/1024.png "App icon")

**ARPokemon** is an augmented reality app that projects a 3D Pokemon object  the live view from a device's camera, once the camera detects a pokemon card. It's developped using Apple's own augmented reality framework [ARKit3](https://developer.apple.com/augmented-reality/arkit/)

The app was developped as an AR practice through udemy's iOS 13 Bootcamp.

